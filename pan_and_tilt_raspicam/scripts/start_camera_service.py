#!/usr/bin/env python
import rospy
from std_srvs.srv import Empty, EmptyRequest

if __name__ == '__main__':
    rospy.init_node('start_camera_service', anonymous=True)
    raspicam_node_service_start = '/pan_and_tilt/raspicam_node/start_capture'
    rospy.logwarn("Waiting for "+raspicam_node_service_start)
    rospy.wait_for_service(raspicam_node_service_start)
    rospy.logwarn(raspicam_node_service_start+"...Ready")
    start_cam = rospy.ServiceProxy(raspicam_node_service_start, Empty)
    request_e = EmptyRequest()
    start_cam(request_e)
    rospy.loginfo("Started Camera Service")