roslaunch raspicam_node camera_module_v2_640x480_30fps.launch
# View Image
rosservice call /raspicam_node/start_capture
# View Strem
rosrun image_view image_view image:=/raspicam_node/image_raw
rosrun rqt_image_view rqt_image_view